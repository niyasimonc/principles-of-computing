"""
Clone of  2048 game.
"""
import poc_2048_gui

import random


# Directions, DO NOT MODIFY
UP = 1
DOWN = 2
LEFT = 3
RIGHT = 4

# Offsets for computing tile indices in each direction.
# DO NOT MODIFY this dictionary.
OFFSETS = {UP: (1, 0),
           DOWN: (-1, 0),
           LEFT: (0, 1),
           RIGHT: (0, -1)}

def slide_the_line(line):
    """
    Function that slides a single row or column to the front of the list
    """
    line_length=len(line)
    slide_of_line=[]
    for index in range(line_length):
        slide_of_line.append(0)
    slide_index=0
    for index in range(line_length):
        if line[index]!=0:
            slide_of_line[slide_index]=line[index]
            slide_index+=1
    return slide_of_line



def merge(line):
    """
    Helper function that merges a single row or column in 2048
    """
    slide_of_line=slide_the_line(line)
    result_line=[]
    index=0
    while index<(len(slide_of_line)):
        if index+1==len(slide_of_line):
            result_line.append(slide_of_line[index])
            break
        if(slide_of_line[index]==slide_of_line[index+1]):
            result_line.append(slide_of_line[index]+slide_of_line[index+1])
            result_line.append(0)
            index+=2
        else:
           result_line.append(slide_of_line[index])
           index+=1
    result_line=slide_the_line(result_line)
    return result_line


class TwentyFortyEight:
    """
    Class to run the game logic.
    """

    def __init__(self, grid_height, grid_width):
        self._grid_height=grid_height
        self._grid_width=grid_width
        self.reset()
        return        


    def reset(self):
        """
        Reset the game so the grid is empty except for two
        initial tiles.
        """
        self._new_grid=[[0 for dummy_col in range(self._grid_width)] for dummy_raw in range(self._grid_height)]
        self.new_tile()
        self.new_tile()
        return        

    def __str__(self):
        """
        Return a string representation of the grid for debugging.
        """
        string=''
        for index in self._new_grid:
            string+=str(index)+'\n'
                
        return string[:-1]
     

    def get_grid_height(self):
        """
        Get the height of the board.
        """
        return self._grid_height

    def get_grid_width(self):
        """
        Get the width of the board.
        """
        return self._grid_width


    def move(self, direction):
        """
        Move all tiles in the given direction and add
        a new tile if any tiles moved.
        """
        initial_tiles={UP:[(0,index) for index in range(self._grid_width)],
                DOWN:[((self._grid_height)-1,index) for index in range(self._grid_width)],
                LEFT:[(index,0) for index in range(self._grid_height)],
                RIGHT:[(index,self._grid_width-1) for index in range(self._grid_height)]}
        end={LEFT:self._grid_width,UP:self._grid_height,RIGHT:self._grid_width,DOWN:self._grid_height}                   
        count=0
        for initial_tile in initial_tiles[direction]:
            temp_list=[(initial_tile)]
            for index in range(1,end[direction]):      
                last=temp_list[len(temp_list)-1]
                temp_list.append((last[0]+OFFSETS[direction][0],last[1]+OFFSETS[direction][1]))
            line=[]

            for index in temp_list:
                line.append(self._new_grid[index[0]][index[1]])
            new_line=merge(line)
            if new_line!=line:
                 count+=1
            value=0

            for index in temp_list:
                self._new_grid[index[0]][index[1]]=new_line[value]
                value+=1
         
        if count>0:
                 self.new_tile() 
 
        return  

    def new_tile(self):
        """
        Create a new tile in a randomly selected empty
        square.  The tile should be 2 90% of the time and
        4 10% of the time.
        """
        while True:
            height=random.choice(range(self._grid_height))
            width=random.choice(range(self._grid_width))
            selection_list=[2,2,2,2,2,2,2,2,2,4]
            if self._new_grid[height][width]==0:
                self._new_grid[height][width]=random.choice(selection_list)
                break
        
        return
              

    def set_tile(self, row, col, value):
        """
        Set the tile at position row, col to have the given value.
        """
        self._new_grid[row][col]=value
        return

    def get_tile(self, row, col):
        """
        Return the value of the tile at position row, col.
      """
        return self._new_grid[row][col]


poc_2048_gui.run_gui(TwentyFortyEight(4, 4))




