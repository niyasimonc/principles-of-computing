"""
Merge function for 2048 game.
"""

def slide_the_line(line):
    """
    Function that slides a single row or column to the front of the list
    """
    line_length=len(line)
    slide_of_line=[]
    for index in range(line_length):
        slide_of_line.append(0)
    slide_index=0
    for index in range(line_length):
        if line[index]!=0:
            slide_of_line[slide_index]=line[index] 
            slide_index+=1
    return slide_of_line   


 
def merge(line):
    """
    Function that merges a single row or column in 2048.
    """
    slide_of_line=slide_the_line(line) 
    result_line=[]
    index=0
    while index<(len(slide_of_line)):
        if index+1==len(slide_of_line):
            result_line.append(slide_of_line[index])
            break
        if(slide_of_line[index]==slide_of_line[index+1]):
            result_line.append(slide_of_line[index]+slide_of_line[index+1])
            result_line.append(0)
            index+=2
        else:
           result_line.append(slide_of_line[index])
           index+=1
    result_line=slide_the_line(result_line)
    return result_line


