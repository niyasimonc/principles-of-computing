"""
Cookie Clicker Simulator
"""

import simpleplot

# Used to increase the timeout, if necessary
import codeskulptor
codeskulptor.set_timeout(20)
import math

import poc_clicker_provided as provided

# Constants
SIM_TIME = 10000000000.0

class ClickerState:
    """
    Simple class to keep track of the game state.
    """
    
    def __init__(self):
        self._total_number_of_cookies=0.0
        self._current_number_of_cookies=0.0
        self._current_time=0.0
        self._CPS=1.0
        self._history=[(0.0,   None,              0.0 ,             0.0)]
                     #(time, item that bought, cost of item, total cookies) ]
        return
        
    def __str__(self):
        """
        Return human readable state
        """
        return  "\nCurrent Time: "+str(self._current_time)+"\nCurrent Number of Cookies: "+str(self._current_number_of_cookies)+"\nCurrent CPS: "+str(self._CPS)+"\nTotal Number of Cookies: "+str(self._total_number_of_cookies)+"\n"
           
                      
    def get_cookies(self):
        """
        Return current number of cookies 
        (not total number of cookies)
        
        Should return a float
        """
        return self._current_number_of_cookies
    
    def get_cps(self):
        """
        Get current CPS

        Should return a float
        """
        return self._CPS
    
    def get_time(self):
        """
        Get current time

        Should return a float
        """
        return self._current_time
    
    def get_history(self):
        """
        Return history list

        History list should be a list of tuples of the form:
        (time, item, cost of item, total cookies)

        For example: [(0.0, None, 0.0, 0.0)]

        Should return a copy of any internal data structures,
        so that they will not be modified outside of the class.
        """
        return self._history

    def time_until(self, cookies):
        """
        Return time until you have the given number of cookies
        (could be 0.0 if you already have enough cookies)

        Should return a float with no fractional part
        """
        
        cookies_to_be_maid=cookies-self._current_number_of_cookies
        seconds_needed=math.ceil(cookies_to_be_maid/self._CPS)
        return seconds_needed
                      
    
    def wait(self, time):
        """
        Wait for given amount of time and update state

        Should do nothing if time <= 0.0
        """
        if(time <= 0.0):
            return
        self._current_time += time
        cookies_maid=time*self._CPS
        self._current_number_of_cookies+=cookies_maid
        self._total_number_of_cookies+=cookies_maid
        return
                      
                      
    
    def buy_item(self, item_name, cost, additional_cps):
        """
        Buy an item and update state

        Should do nothing if you cannot afford the item
        """
        if cost<=self._current_number_of_cookies:
             self._current_number_of_cookies-=cost     
             self._CPS+=additional_cps
             self._history.append((self._current_time,item_name,cost,self._total_number_of_cookies))
        return
             
   
    
def simulate_clicker(build_info, duration, strategy):
    """
    Function to run a Cookie Clicker game for the given
    duration with the given strategy.  Returns a ClickerState
    object corresponding to the final state of the game.
    """
    copy_buildinfo = build_info.clone()
    clickstate = ClickerState()
    
    while(clickstate.get_time() <= duration):
        item = strategy(clickstate.get_cookies(), clickstate.get_cps(), clickstate.get_history(), duration-clickstate.get_time(), copy_buildinfo)
        if(item == None):
            break
        time = clickstate.time_until(copy_buildinfo.get_cost(item))
        if((clickstate.get_time()+time) > duration):
            break
        clickstate.wait(time)
     
        while(copy_buildinfo.get_cost(item) <= clickstate.get_cookies()):
            clickstate.buy_item(item, copy_buildinfo.get_cost(item), copy_buildinfo.get_cps(item))
            copy_buildinfo.update_item(item)
                
    clickstate.wait(duration - clickstate.get_time())
    

    return clickstate

    

    
    
def strategy_cursor_broken(cookies, cps, history, time_left, build_info):
    """
    Always pick Cursor!

    Note that this simplistic (and broken) strategy does not properly
    check whether it can actually buy a Cursor in the time left.  Your
    simulate_clicker function must be able to deal with such broken
    strategies.  Further, your strategy functions must correctly check
    if you can buy the item in the time left and return None if you
    can't.
    """
    return "Cursor"

def strategy_none(cookies, cps, history, time_left, build_info):
    """
    Always return None

    This is a pointless strategy that will never buy anything, but
    that you can use to help debug your simulate_clicker function.
    """
    return None

def strategy_cheap(cookies, cps, history, time_left, build_info):
    """
    Always buy the cheapest item you can afford in the time left.
    """
    items = build_info.build_items()
    
    costs =  [build_info.get_cost(item) for item in items]
    
     
    min_cost =  min(costs)
    min_ind = costs.index(min_cost)
    if((cookies+cps*time_left) >= min_cost):
        
        return items[min_ind]
        
    
    return None
  

def strategy_expensive(cookies, cps, history, time_left, build_info):
    """
    Always buy the most expensive item you can afford in the time left.
    """
    items = build_info.build_items()
   
    costs =  [build_info.get_cost(item) for item in items]
   
    proj_budget = cookies+cps*time_left
    affordable_costs = filter(lambda x: x <= proj_budget, costs)
    if not(affordable_costs):
        return None
    affordable_idx = [costs.index(val) for val in affordable_costs]
    affordable_items = [items[dummy_x] for dummy_x in affordable_idx]
    
    max_cost =  max(affordable_costs)
    
    
    return affordable_items[affordable_costs.index(max_cost)]

def strategy_best(cookies, cps, history, time_left, build_info):
    """
    The best strategy that you are able to implement.
    """
    return min([(float("inf"), None)] + [(build_info.get_cost(item) * ( 1 / cps + 1 / build_info.get_cps(item)), item) for item in build_info.build_items() if build_info.get_cost(item) <= (cookies + cps * time_left) / 1.5])[1]
    
    
    
         
def run_strategy(strategy_name, time, strategy):
    """
    Run a simulation for the given time with one strategy.
    """
    state = simulate_clicker(provided.BuildInfo(), time, strategy)
    print strategy_name, ":",state

    # Plot total cookies over time

    # Uncomment out the lines below to see a plot of total cookies vs. time
    # Be sure to allow popups, if you do want to see it

    # history = state.get_history()
    # history = [(item[0], item[3]) for item in history]
    # simpleplot.plot_lines(strategy_name, 1000, 400, 'Time', 'Total Cookies', [history], True)

def run():
    """
    Run the simulator.
    """    
   # run_strategy("Cursor", SIM_TIME, strategy_cursor_broken)

    # Add calls to run_strategy to run additional strategies
    # run_strategy("Cheap", SIM_TIME, strategy_cheap)
    # run_strategy("Expensive", SIM_TIME, strategy_expensive)
    run_strategy("Best", SIM_TIME, strategy_best)
    
run()


