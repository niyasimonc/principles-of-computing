"""
Planner for Yahtzee
Simplifications:  only allow discard and roll, only score against upper level
"""

# Used to increase the timeout, if necessary
#import codeskulptor
from itertools import combinations
#codeskulptor.set_timeout(20)

def gen_all_sequences(outcomes, length):
    """
    Iterative function that enumerates the set of all sequences of
    outcomes of given length.
    """
    
    answer_set = set([()])
    for dummy_idx in range(length):
        temp_set = set()
        for partial_sequence in answer_set:
            for item in outcomes:
                new_sequence = list(partial_sequence)
                new_sequence.append(item)
                temp_set.add(tuple(new_sequence))
        answer_set = temp_set
    return answer_set


def score(hand):    
    """
    Compute the maximal score for a Yahtzee hand according to the
    upper section of the Yahtzee score card.
    hand: full yahtzee hand
    Returns an integer score 
    """    
    hand_list=list(hand)
    elements=set(hand)
    score_=[idx*hand_list.count(idx) for idx in elements]
    score_.sort()        
    return score_[-1]


def expected_value(held_dice, num_die_sides, num_free_dice):
    """
    Compute the expected value based on held_dice given that there
    are num_free_dice to be rolled, each with num_die_sides.

    held_dice: dice that you will hold
    num_die_sides: number of sides on each die
    num_free_dice: number of dice to be rolled

    Returns a floating point expected value
    """
    all_sequences=list(gen_all_sequences(range(1,num_die_sides+1),num_free_dice))
    dice_list=[held_dice+idx for idx in all_sequences]
    score_list=[]
    for idx in dice_list:
        scores=score(idx)
        score_list.append(scores)
        
    probability=1.0/(num_die_sides**num_free_dice)
    exp_val=0
    for idx in score_list:
        exp_val+=idx*probability  
    return exp_val





def gen_all_holds(hand):
    """
    Generate all possible choices of dice from hand to hold.

    hand: full yahtzee hand

    Returns a set of tuples, where each tuple is dice to hold
    """
    subset=[]
    for idx in range(len(hand)+1):
        subset.append(tuple(combinations(hand, idx)))
    subsets=set([])
    for tuples in  subset:
        for idx in range(len(tuples)):
            subsets.add(tuples[idx])
        
    return subsets
    
    



def strategy(hand, num_die_sides):
    """
    Compute the hold that maximizes the expected value when the
    discarded dice are rolled.

    hand: full yahtzee hand
    num_die_sides: number of sides on each die

    Returns a tuple where the first element is the expected score and
    the second element is a tuple of the dice to hold
    """
    possible_sequences=list(gen_all_holds(hand))
    maxim=0.0
    held_dice=[]
    for sequence in possible_sequences:
        expect_value=expected_value(sequence,num_die_sides,len(hand)-len(sequence))
        if expect_value>maxim:
            maxim=expect_value
        held_dice.append((expect_value,sequence))
    held=[]
    for tuples in held_dice:
        if tuples[0]==maxim:
            held.append(tuples[1])

    return (maxim, tuple(held))


def run_example():
    """
    Compute the dice to hold and expected score for an example hand
    """
    num_die_sides = 6
    hand = (1, 1, 1, 1, 1)
    hand_score, hold = strategy(hand, num_die_sides)
    print "Best strategy for hand", hand, "is to hold", hold, "with expected score", hand_score
    
#print strategy((1,2,3,4,4),6)    
#print len(gen_all_holds((1,2,4,4)))
#print expected_value((3,3),6,1)
#print score((1,1,2,2,2,2,3,3))
run_example()


#import poc_holds_testsuite
#poc_holds_testsuite.run_suite(gen_all_holds)
                                       
    
    
    



