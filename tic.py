"""
Monte Carlo Tic-Tac-Toe Player
"""

import random
import poc_ttt_gui
import poc_ttt_provided as provided

# Constants for Monte Carlo simulator
# You may change the values of these constants as desired, but
#  do not change their names.
NTRIALS = 250         # Number of trials to run
SCORE_CURRENT = 1.0 # Score for squares played by the current player
SCORE_OTHER = 1.0   # Score for squares played by the other player
    
def mc_trial(board,player):
    """
    This function takes a current board and the next player to move.The function should 
    play a game starting with the given player by making random moves,alternating between players. The
    function should return when the game is over.The modified board will contain the state of the game, so 
    the function does not return anything. In other words, the function should modify the board input.
    """
    while(board.check_win()==None):
        row=random.randint(0,board.get_dim()-1)
        col=random.randint(0,board.get_dim()-1)
        board.move(row,col,player)
        player=provided.switch_player(player)
        
        
        
        
    
def mc_update_scores(scores, board, player):    
    """
    This function takes a grid of scores (a list of 
    lists) with the same dimensions as the Tic-Tac-Toe 
    board, a board from a completed game, and which 
    player the machine player is. The function should 
    score the completed board and update the scores grid.
    As the function updates the scores grid directly, 
    it does not return anything,
    """
    winner=board.check_win()  
    dim = board.get_dim()
    man = provided.switch_player(player)
    
    while winner!=provided.DRAW and winner!=None:
        for row in range(dim):
            for col in range(dim):
                if(winner == player):
                    if (board.square(row, col) == player):
                        scores[row][col]+=SCORE_CURRENT
                    elif(board.square(row, col) == man):
                        scores[row][col]+=(-SCORE_OTHER)
                else:
                    if (board.square(row, col) == player):
                        scores[row][col]+=(-SCORE_CURRENT)
                    elif(board.square(row, col) == man):
                        scores[row][col]+=SCORE_OTHER
        break
        
        

            
def get_best_move(board,scores):
    
    """
    This function takes a current board and a grid of scores. The 
    function should find all of the empty squares with the maximum 
    score and randomly return one of them as a (row, column) tuple.
    It is an error to call this function with a board that has no 
    empty squares (there is no possible next move), so your 
    function may do whatever it wants in that case. The case where 
    the board is full will not be tested.
    """
  
    empty_squares=board.get_empty_squares()
    score=[]
    for row in range(len(scores)):
        for col in range(len(scores)):
            score.append(scores[row][col])
    score.sort()
    score.reverse()
    if len(empty_squares):
        for value in score:
           big=value
           values=[]
           for row in range(len(scores)):
            for col in range(len(scores)):
                if scores[row][col]==big and board.square(row,col)==provided.EMPTY:
                      values.append((row,col))
            if len(values):
                return random.choice(values)
            
def mc_move(board, player, trials):
    """
    This function takes a current board, which player the machine player is, and the number of trials
    to run. The function should use the Monte Carlo simulation described above to return a move for the 
    machine player in the form of a (row, column) tuple. Be sure to use the other functions you have written!
    """

    score=[[ 0 for dummy_num in range(board.get_dim())]for dummy_num in range(board.get_dim())]
    for dummy_index in range(trials):
        dummy_board = board.clone()
        mc_trial(dummy_board, player)
        mc_update_scores(score, dummy_board, player)
    return get_best_move(board, score)
                       
            
            
            
print get_best_move(provided.TTTBoard(2, False, [[provided.EMPTY, provided.PLAYERX], [provided.EMPTY, provided.PLAYERO]]), [[3, 3], [0, 0]]) 
          
            
            


# Test game with the console or the GUI.  Uncomment whichever 
# you prefer.  Both should be commented out when you submit 
# for testing to save time.

# provided.play_game(mc_move, NTRIALS, False)        
 #poc_ttt_gui.run_gui(3, provided.PLAYERX, mc_move, NTRIALS, False)

